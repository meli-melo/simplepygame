import pygame
from pygame.locals import *

class Floor:
	sprite = "floor.jpg"
	def __init__(self, sprite, windowWidth, windowHeight):
		self.displaySurface = None
		self.floorArray = []
		self.floorTile = pygame.transform.scale(pygame.image.load(sprite).convert(), (50, 50))
		for i in range(0, int(windowWidth/50)+1):
			tempRect = self.floorTile.get_rect()
			tempRect.x = i*50
			tempRect.y = windowHeight-50
			self.floorArray.append(tempRect)
			
	def setDisplaySurface(self, surface):
		self.displaySurface = surface

	def display(self, window):
		for rect in self.floorArray:
			self.displaySurface.blit(self.floorTile,  rect)


