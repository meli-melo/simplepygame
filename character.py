import pygame
from pygame.locals import *
from spell import Spell

class Character:
	sprite = "perso.png"
	moveSpeed = 8
	velocity =  8
	mass = 2
	def __init__(self, size, sprite, windowHeight):
		self.displaySurface = None
		self.size = size
		self.character = pygame.transform.scale(pygame.image.load(sprite).convert_alpha(), (size,size))
		self.pos = self.character.get_rect()
		self.pos = self.pos.move(0, windowHeight-50-50)
		self.isJumping = False
		self.spell = Spell()
		self.isAttacking = False

	def setDisplaySurface(self, surface):
		self.displaySurface = surface
		self.spell.setDisplaySurface(surface)

	def display(self ):
		self.displaySurface.blit(self.character, self.pos)

	def jump(self):
		self.isJumping = True

	def attack(self):
		self.isAttacking = True
		self.attackStartTime = pygame.time.get_ticks()
		
	def moveRight(self):
		if not self.isAttacking:
			self.pos = self.pos.move(self.moveSpeed, 0)

	def moveLeft(self):
		if not self.isAttacking:
			self.pos = self.pos.move(-self.moveSpeed, 0)

	def update(self, floorArray):
		if self.isAttacking:
			#is attacking during the jump, cancel the rest of the jump
			if self.isJumping:
				self.velocity = 0

			 #calculate how many milli seconds
			milliSec = (pygame.time.get_ticks() - self.attackStartTime)
			
 # if more than the attack anim length
			if milliSec > self.spell.animTime:
				self.spell.stopAnim()
				self.isAttacking = False
			else:
				self.spell.display(self.pos.move(50, -25))
		else:
			if self.isJumping:
				tempPos = self.pos
				#Calculate force F = 0.5 * mass * velocity ^2
				if self.velocity > 0:
					F = (0.5 * self.mass * (self.velocity * self.velocity))
				else:
					F = -(0.5 * self.mass * (self.velocity * self.velocity))
				tempPos.y -= F
				self.velocity -= 1.5
				grounded = False
				for floorRect in floorArray:
					if tempPos.colliderect(floorRect):
						grounded = True
						self.pos.y = floorRect.y - 50
						print(self.pos)
						print(floorRect)

						self.isJumping = False
						self.velocity = 8
						break

				if not grounded:
					self.pos = tempPos
		self.display()
