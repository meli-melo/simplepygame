import pygame
from pygame.locals import *
import pyganim

class Spell:
	def __init__(self):
		self.displaySurface = None
		self.isPlaying = False
		self.animFrames = 10
		self.animTime = 900
		self.spriteAnim = pyganim.PygAnimation([('bolt_strike_0001.png', 100),                ('bolt_strike_0002.png', 100),('bolt_strike_0003.png', 100),('bolt_strike_0004.png', 100),('bolt_strike_0005.png', 100),('bolt_strike_0006.png', 100),('bolt_strike_0007.png', 100),('bolt_strike_0008.png', 100),('bolt_strike_0009.png', 100),('bolt_strike_0010.png', 100)])

	def setDisplaySurface(self, surface):
		self.displaySurface = surface
	
	def display(self, pos):
		if not self.isPlaying:
			self.spriteAnim.play()
			self.isPlaying = True
		self.spriteAnim.blit(self.displaySurface, pos)

	def stopAnim(self):
		self.spriteAnim.stop()
		self.isPlaying = False;
		
