import pygame
from pygame.locals import *
from character import Character
from floor import Floor
from time import sleep
import pyganim

class App:
	windowWidth = 1600
	windowHeight = 800
	player = 0

	def __init__(self):
		self.running = True
		self.displaySurface = None
		self.imageSurface = None
		self.imageBackground = None

	def on_init(self):
		pygame.init()
		self.displaySurface = pygame.display.set_mode((self.windowWidth, self.windowHeight), RESIZABLE)
		self.player = Character(50, "perso.png", self.windowHeight)
		pygame.display.set_caption('Test game')
		self.running = True
		self.imageBackground = pygame.transform.scale(pygame.image.load("background.jpg").convert(), (self.windowWidth, self.windowHeight))
		self.floor =  Floor("floor.jpg", self.windowWidth, self.windowHeight)
		self.player.setDisplaySurface(self.displaySurface)
		self.floor.setDisplaySurface(self.displaySurface)
		#self.imageSurface = pygame.image.load
		#self.boltAnim = pyganim.PygAnimation([('bolt_strike_0001.png', 100),                ('bolt_strike_0002.png', 100),('bolt_strike_0003.png', 100),('bolt_strike_0004.png', 100),('bolt_strike_0005.png', 100),('bolt_strike_0006.png', 100),('bolt_strike_0007.png', 100),('bolt_strike_0008.png', 100),('bolt_strike_0009.png', 100),('bolt_strike_0010.png', 100)])
		#self.boltAnim.play()

	def on_event(self):
		if event.type == QUIT:
			self.running = False

	def on_loop(self):
		pass

	def on_render(self):
		self.displaySurface.blit(self.imageBackground, (0, 0))
		self.player.update(self.floor.floorArray)
		self.floor.display(self.displaySurface)
		#self.boltAnim.blit(self.displaySurface, (100, 50))
		pygame.display.flip()
		sleep(0.03)

	def on_cleanup(self):
		pygame.quit()

	def on_execute(self):
		if self.on_init() == False:
			self.running = False

		while self.running:
			pygame.event.pump()
			keys = pygame.key.get_pressed()

			if keys[K_RIGHT]:
				self.player.moveRight()
			if keys[K_LEFT]:
				self.player.moveLeft()
			if keys[K_UP] :
				self.player.jump()
			if keys[K_ESCAPE]:
				self.running = False
			if keys[K_SPACE]:
				self.player.attack()

			self.on_loop()
			self.on_render()
		self.on_cleanup()

theApp = App()
theApp.on_execute()
